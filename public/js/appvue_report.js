const monthNames = [
    "Januari", "Februari", "Maret",
    "April", "Mei", "Juni", "Juli",
    "Agustus", "September", "Oktober",
    "November", "Desember"
];
const years = ['2020','2021']

var rptAbsensi = new Vue({
    el: '#report',
    data: {
      hidetgl: false,
      monthSelected: 'Desember',
      yearSelected: '2020',
      pilihtgl: true,
      unitkerja: null,
      ukSelected: null,
      url: "https://apps.mkri.id/",
      absensipegawai: null,
      tgl_awal: '2020-09-16',
      tgl_akhir: '2020-10-15',
      jenispegawai: 'all',
      boolmangkir: false,
    },
    methods: {
      getInits: function() {
        let self = this;

        // ambil data unit kerja
        axios.get(self.url + 'api/master/unitkerja')
        .then (function(response) {
          console.log("Berhasil ambil init bulan!");
          console.log(response.data);
          self.unitkerja = response.data;      
        })
        .catch(function(error) {
          console.log("Gagal ambil init unit kerja: " + error);
        })

      },
      showPilih: function() {
        let self = this;

        // reset
        self.absensipegawai = null;
        self.pilihtgl = true;

      },
      showReport: function() {
        let self = this;

        self.pilihtgl = false;
        self.runreport();
      },
      toshortDate: function(date) {
          let tanggal = date.split(' ')[0].split('-');

          date = new Date(
              parseInt(tanggal[0]),
              parseInt(tanggal[1])-1,
              parseInt(tanggal[2]));

          let day = date.getDate();
          let month = date.getMonth() + 1;
          let year = date.getFullYear();

          if(day<10) day='0'+day;
          if(month<10) month='0'+month;

          return day + '-' + month + '-' + year;
      },
      ambilTanggal: function (date) {

          // Supported by all browser kit:
          // ======================================================================================
          // var d = new Date(2011, 01, 07); // yyyy, mm-1, dd
          // var d = new Date(2011, 01, 07, 11, 05, 00); // yyyy, mm-1, dd, hh, mm, ss
          // var d = new Date("02/07/2011"); // "mm/dd/yyyy"
          // var d = new Date("02/07/2011 11:05:00"); // "mm/dd/yyyy hh:mm:ss"
          // var d = new Date(1297076700000); // milliseconds
          // var d = new Date("Mon Feb 07 2011 11:05:00 GMT"); // ""Day Mon dd yyyy hh:mm:ss GMT/UTC
          // ======================================================================================

          if(date !== null)
          {

              let tanggal = date.split(' ')[0].split('-');

              date = new Date(
                  parseInt(tanggal[0]),
                  parseInt(tanggal[1])-1,
                  parseInt(tanggal[2]));

              let day = date.getDate();
              // if(day<10) day='0'+day;

              return day;
          }
          else {
              return '-';
          }
      },
      runreport: function() {
        let self = this;

        if(self.ukSelected == null) self.ukSelected = 'Biro Umum';  // default
        if(self.jenispegawai == 'all' || self.jenispegawai == 'ppnpn' || self.ukSelected !== 'Kontrak') self.jenispegawai = 'asn';
        if(self.ukSelected == 'Kontrak') self.jenispegawai = 'kontrak';

        self.absensipegawai = null;

        // axios.get(self.url + 'api/absenwfh/report/' + awal + '/' + akhir)
        axios.get(self.url + 'api/absenwfh/report/' + self.jenispegawai + '/' + self.ukSelected + '/' + self.tgl_awal + '/' + self.tgl_akhir)
        .then (function(response) {
          console.log(response.data.hasil);
          console.log("Berhasil ambil absensi!");
          // self.absensipegawai = [];
          self.absensipegawai = response.data.hasil.pegawai;
          self.boolmangkir = response.data.date_mangkir;

          // self.absensipegawai.forEach((item, i) => {
          //   if(self.unitkerja.indexOf(item.unit_kerja) === -1 && item.unit_kerja !== 'Sekretariat Jenderal') self.unitkerja.push(item.unit_kerja);
          // });

        })
        .catch(function(error) {
          console.log("Gagal ambil absensi: " + error);
        })

      },


    },
    computed: {
      filteredAsn() {
        let self = this;

        return self.filterjenispegawai.filter((pegawai) => {
            return pegawai.unit_kerja === self.ukSelected;
        })
      },
      filterjenispegawai() {
          let self = this;

          return self.absensipegawai.filter((pegawai) => {
            // if(self.jenispegawai === 'all') return pegawai;
            if(self.jenispegawai === 'asn') return pegawai.pgw_id <= 2000;
            if(self.jenispegawai === 'ppnpn') return pegawai.pgw_id > 2000 && pegawai.pgw_id < 5000;
            if(self.jenispegawai === 'kontrak') return pegawai.pgw_id > 5000;
          })
        },
      bulanWfh() {
          let self = this;
          let bln = new Date().getMonth();

          return monthNames.filter((bulan, index) => {
              return ( index >= 2 && self.yearSelected == '2020') ||
                     ( index <= bln && self.yearSelected > '2020');
          })
      },
    },
  
})



$(window).load(function () {
  rptAbsensi.getInits();
});
