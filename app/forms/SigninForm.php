<?php

namespace App\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Email;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Submit;
use Phalcon\Validation\Validator\PresenceOf;

class SigninForm extends Form
{
    public function initialize()
    {
        
         // Form Email field
        $email = new Email(
            'email',
            [
                "class" => "form-control",
                "placeholder" => "Enter Email Address"
            ]
        );
         // Form Email
         $password = new Password(
            'password',
            [
                "class" => "form-control",
                "placeholder" => "Enter Password"
            ]
        );

        // Form submit button
        $submit = new Submit(
            'submit',
            [
                "value" => "Sign In",
                "class" => "btn btn-lg btn-warning btn-block mt-5"
            ]
        );

        // Form name field validation
        $email->addValidator(
            new PresenceOf(['message' => 'The email is not valid'])
        );

        $this->add($password);
        $this->add($email);
        $this->add($submit);
    }
}