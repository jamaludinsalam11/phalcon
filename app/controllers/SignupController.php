<?php
use Phalcon\Mvc\Controller;
use Phalcon\Http\Request;
use Phalcon\Security;

//use Form
use App\Forms\RegisterForm;
class SignupController extends  Controller
{

    public function indexAction()
    {
       $this->view->form = new RegisterForm();
    }

    public function registerAction()
    {
        // Getting a request instance
        $request = new Request();

        // var_dump($request->isPost());
        // var_dump($request->isAjax());
        // exit;
        $user = new Users();
        $form = new RegisterForm();

        //check request
        if(!$this->request->isPost()){
            return $this->response->redirect('signup');
        }

        //Check from validation
        if(!$form->isValid($_POST)){
            $messages = $form->getMessages();
            foreach($messages as $message){
                $this->flashSession->error($message);
                return $this->response->redirect('signup');
            }
        }

        $email = $this->request->getPost('email', ['trim', 'email']);
        $password = $this->request->getPost('password', ['trim', 'string']);
        $security = new Security();
        $passwordHashed = $security->hash($password);
       

        // Store and check for errors
        $success = $user->save([
            "email" => $email,
            "password" => $passwordHashed
        ]);

        if ($success) {
            
            // Direct
            // echo $this->flash->success('User was Registered');
            $this->flashSession->success('Your information was stored correctly!');
            //Forward to the index action
            return $this->response->redirect('signin');
        } else {
            echo "Sorry, the following problems were generated: ";

            $messages = $user->getMessages();

            //Forward to the index action
            
            foreach ($messages as $message) {
                echo  $this->flashSession->error($message); "<br/>";
            }
            return $this->response->redirect('signup');
        }

        $this->view->disable();
    }

}

