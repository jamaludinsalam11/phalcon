<?php
use Phalcon\Mvc\Controller;
use Phalcon\Http\Request;
use Phalcon\Security;
use Phalcon\Mvc\Url;
//use Form
use App\Forms\SigninForm;

class SigninController extends  Controller
{
    public $loginForm;
    public $usersModel;

    public function onConstruct()
    {
        
    }

    public function initialize()
    {

        $this->signinForm = new SigninForm();
        $this->usersModel = new Users();
    }

    public function indexAction()
    {
       $this->view->form = new SigninForm();
       $this->tag->setTitle('Phalcon :: Login');
        // Login Form
        $this->view->form = new SigninForm();
      


$url = new Url();

echo $url->getBaseUri();
exit;
    }


    public function loginAction()
    {
        $user = new Users();
        $form = new SigninForm();
        
        //Check request
        if(!$this->request->isPost()){
            return $this->response->redirect('signin');
        }

        //Check from validation
        if(!$form->isValid($_POST)){
            $messages = $form->getMessages();
            foreach($messages as $message){
                $this->flashSession->error($message);
                return $this->response->redirect('signin');
            }
        }

        $form->bind($_POST, $user);
        //Check form validation
        $email = $this->request->getPost('email', ['trim', 'email']);
        $password = $this->request->getPost('password', ['trim', 'string']);
        $security = new Security();
        $passwordHashed = $security->hash($password);
       
       $user = Users::findFirstByEmail($email);
       if($user){
           if( $this->security->checkHash($password, $user->password)){
               $this->session->set("AUTH_EMAIL", $user);
               $this->flashSession->success('Login Success');
               return $this->response->redirect('absensi');
           } else{
                $this->flashSession->error('Invalid Password');
                return $this->response->redirect('signin');
           }
       } else {
           echo $this->security->hash(rand());
       }
        
   

        // if(false !== $user){
        //     $check = $this
        //         ->security
        //         ->checkHash($password, $user->password);

        //         if(true === $check){
        //             echo 'logiinn';
        //         } else {
        //             echo $this->security->hash(rand());
        //         }
        // }
    }

}